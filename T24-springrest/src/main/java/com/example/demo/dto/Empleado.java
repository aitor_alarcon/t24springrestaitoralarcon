package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="empleados")
public class Empleado {

	//Atributos de entidad cliente
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;
		@Column(name = "nombre")
		private String nombre;
		@Column(name = "trabajo")
		private String trabajo;
		@Column(name = "salario")
		private Integer salario;
		
		//Constructores

		public Empleado() {
			
		}

		/**
		 * @param id
		 * @param nombre
		 * @param trabajo
		 */
		
		public Empleado(Integer id, String nombre, String trabajo) {
			//super();
			this.id = id;
			this.nombre = nombre;
			this.trabajo = trabajo;
			this.salario = 0;
		}
		
		/**
		 * @param id
		 * @param nombre
		 * @param trabajo
		 * @param salario
		 */
		
		public Empleado(Integer id, String nombre, String trabajo, Integer salario) {
			//super();
			this.id = id;
			this.nombre = nombre;
			this.trabajo = trabajo;
			this.salario = salario;
		}

		//Getters y Setters
		
		/**
		 * @return the id
		 */
		public Integer getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(Integer id) {
			this.id = id;
		}

		/**
		 * @return the nombre
		 */
		public String getNombre() {
			return nombre;
		}

		/**
		 * @param nombre the nombre to set
		 */
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		/**
		 * @return the trabajo
		 */
		public String getTrabajo() {
			return trabajo;
		}

		/**
		 * @param trabajo the trabajo to set
		 */
		public void setTrabajo(String trabajo) {
			this.trabajo = trabajo;
		}
		
		/**
		 * @return the salario
		 */
		public Integer getSalario() {
			return salario;
		}

		/**
		 * @param salario the salario to set
		 */
		public void setSalario(Integer salario) {
			this.salario = salario;
		}

		//toString
		@Override
		public String toString() {
			return "Empleado [id=" + id + ", nombre=" + nombre + ", trabajo=" + trabajo + ", salario=" + salario + "]";
		}

		// Método para asignar un salario a un puesto de trabajo
		
		public static Integer asignarSalario(String trabajo) {
			
			int salario = 0;
			
			if(trabajo.contentEquals("programador")) {
				salario = 2000;
			}else if(trabajo.contentEquals("tester")) {
				salario = 2500;
			}else if(trabajo.contentEquals("administrativo")) {
				salario = 1200;
			}else if(trabajo.contentEquals("director")) {
				salario = 1000;
			}
			
			return salario;
			
		}

		
		
}
