package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T24SpringrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(T24SpringrestApplication.class, args);
	}

}
