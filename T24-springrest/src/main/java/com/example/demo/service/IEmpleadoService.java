package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Empleado;

public interface IEmpleadoService {

		//Metodos del CRUD
		public List<Empleado> listarEmpleados();
		
		public Empleado guardarEmpleado(Empleado empleado);
		
		public Empleado empleadoID(Integer id);
		
		public List<Empleado> empleadoTrabajo(String trabajo);
		
		public Empleado actualizarEmpleado(Empleado empleado); 
		
		public void eliminarEmpleado(Integer id);
}
