package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Empleado;
import com.example.demo.service.EmpleadoServiceImpl;

@RestController
@RequestMapping("/api")
public class EmpleadoController {
	
	@Autowired
	EmpleadoServiceImpl empleadoServiceImpl;
	
	@GetMapping("/empleados")
	public List<Empleado> listarEmpleados(){
		return empleadoServiceImpl.listarEmpleados();
	}
	
	@PostMapping("/empleados")
	public Empleado guardarEmpleado(@RequestBody Empleado empleado) {
		return empleadoServiceImpl.guardarEmpleado(empleado);
	}
	
	@GetMapping("/empleados/{id}")
	public Empleado empleadoID(@PathVariable(name="id") Integer id) {
		Empleado empleadoID = new Empleado();
		
		empleadoID = empleadoServiceImpl.empleadoID(id);
		
		System.out.println("Empleado ID: " + empleadoID);
		
		return empleadoID;
		
	}
	
	@GetMapping("/empleados/trabajo/{trabajo}")
	public List<Empleado> empleadoTrabajo(@PathVariable(name = "trabajo") String trabajo) {
		return empleadoServiceImpl.empleadoTrabajo(trabajo);
	}

	
	@PutMapping("/empleados/{id}")
	public Empleado actualizarEmpleado(@PathVariable(name="id") Integer id, @RequestBody Empleado empleado) {
		
		Empleado empleado_sel = new Empleado();
		Empleado empleado_act = new Empleado();
		
		empleado_sel = empleadoServiceImpl.empleadoID(id);
		
		empleado_sel.setId(empleado.getId());
		empleado_sel.setNombre(empleado.getNombre());
		empleado_sel.setTrabajo(empleado.getTrabajo());
		empleado_sel.setSalario(com.example.demo.dto.Empleado.asignarSalario(empleado.getTrabajo()));
		
		empleado_act = empleadoServiceImpl.actualizarEmpleado(empleado_sel);
		
		System.out.println("El empleado actualizado está así = " + empleado_act);
		
		return empleado_act;
		
	}
	
	@DeleteMapping("/empleados/{id}")
	public void eliminarEmpleado(@PathVariable(name="id") Integer id) {
		empleadoServiceImpl.eliminarEmpleado(id);
	}
	
}
