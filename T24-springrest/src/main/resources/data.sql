DROP table IF EXISTS empleados CASCADE;

CREATE TABLE `empleados`(
`id` int(11) auto_increment,
`nombre` varchar(250),
`trabajo` ENUM('tester','director','administrativo','programador'),
`salario` integer,
PRIMARY KEY (`id`)
);                              

insert into empleados (id, nombre, trabajo, salario)values(1,'Aitor','programador', 1500);
insert into empleados (id, nombre, trabajo, salario)values(2,'Alberto','tester', 1500);
insert into empleados (id, nombre, trabajo, salario)values(3,'Oscar','administrativo', 2000);
insert into empleados (id, nombre, trabajo, salario)values(4,'Dani','programador', 3000);
insert into empleados (id, nombre, trabajo, salario)values(5,'Pau','administrativo', 1000);